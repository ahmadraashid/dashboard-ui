import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CoolLocalStorage } from 'angular2-cool-storage';
import * as urlsData from 'configs.json';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
declare var moment: any;

@Injectable()
export class TehsilService {
    localStorage: CoolLocalStorage;
    private baseUrl = '';
    private districtsUrl = '';
    private tehsilsUrl = '';
    private districtTehsilsSummaryUrl = '';

    public constructor(private http: Http, localStorage: CoolLocalStorage) {
        
        this.localStorage = localStorage;
        this.loadConfigs(this.localStorage);

        var urls = this.localStorage.getItem('api_urls');
        if (urls) {
            var urlsList = JSON.parse(urls.toString());
            this.baseUrl = urlsList.baseUrl;
            this.districtsUrl = this.baseUrl + urlsList.districtsList;
            this.tehsilsUrl = this.baseUrl + urlsList.tehsilsList;
            this.districtTehsilsSummaryUrl = this.baseUrl + urlsList.districtTehsilsSummary;
        } 
    }

    public getDistricts() {
        var options = this.getRequestHeaders();
        return this.http.get(this.districtsUrl, options).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    public getTehsils() {
        var options = this.getRequestHeaders();
        return this.http.get(this.tehsilsUrl, options).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    public getSelectedDistrictSummary(dateFilter) {
        var options = this.getRequestHeaders();
        let body = JSON.stringify(dateFilter);

        return this.http.post(this.districtTehsilsSummaryUrl, body, options)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    private getRequestHeaders() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    private loadConfigs(storage) {
        storage.setItem("api_urls", JSON.stringify((<any>urlsData).apiUrls));
    }
}