import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

Injectable()
export class StoreService {
    public openDateTime: Subject < Boolean >;
    public searchFard: Subject < Number >;
    public currentDate: String = '';
    private messageDisplayTime = 7000;

    private colors = [
        "#D98880", "#7D3C98", "#4cc6bb", "#fdd100", "#123ea9", "#FF0000",
        "#800000", "#1ABC9C", "#154360", "#F1C40F", "#5D6D7E", "#A04000"];

    constructor () {
        this.openDateTime = new Subject <Boolean>();
        this.searchFard = new Subject <Number>();
    }

    public setDateTimeStatus(status: Boolean): void {
        this.openDateTime.next(status);
    }

    public setNewDate(newDate: String): void {
        var dtArr = newDate.split('/');
        var dbDateFormat = dtArr[2] + '-' + dtArr[0] + '-' + dtArr[1];
        this.currentDate = dbDateFormat;
    }

    public getCurrentDate() : string {
        var dt = new Date();
        var dtInSqlServerFormat = dt.getFullYear() + '-' + this.padTwoDigits((dt.getMonth() + 1)) + '-' 
        + this.padTwoDigits(dt.getDate());
        return dtInSqlServerFormat;
    }

    padTwoDigits(number): string {
        return (number < 10 ? '0' : '') + number;
     }

    public getId(element) {
        var target = element.target || element.srcElement || element.currentTarget;
        var idAttr = target.attributes.id;
        var itemId = 0;
        if (idAttr.nodeValue)
            itemId = parseInt(idAttr.nodeValue.split('-')[1]);
        else
            itemId = parseInt(idAttr.toString().split('-')[1]);

        return itemId;
    }

    public getColor(index) : string {
        if (index < 0 || index > this.colors.length) {
            return '#000000';
        } else
        return this.colors[index];
    }

    public getMessageDisplayTime() {
        return this.messageDisplayTime;
    }

}