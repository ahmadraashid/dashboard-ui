import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CoolLocalStorage } from 'angular2-cool-storage';
import * as urlsData from 'configs.json';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
declare var moment: any;

@Injectable()
export class FardService {
    localStorage: CoolLocalStorage;
    private baseUrl = '';
    private fardSummaryUrl = '';
    

    public constructor(private http: Http, localStorage: CoolLocalStorage) {
        this.localStorage = localStorage;
        this.loadConfigs(this.localStorage);

        var urls = this.localStorage.getItem('api_urls');
        if (urls) {
            var urlsList = JSON.parse(urls.toString());
            this.baseUrl = urlsList.baseUrl;
            this.fardSummaryUrl = this.baseUrl + urlsList.fardSummaries;
        }
    }

    public getFardSummaries(dataFilter) {
        var options = this.getRequestHeaders();
        let body = JSON.stringify(dataFilter);

        return this.http.post(this.fardSummaryUrl, body, options)
        .map((res) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    private getRequestHeaders() {
        //var accessToken = this.localStorage.getItem('access_token');
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        //headers.append('Authorization', 'Bearer ' + accessToken);
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    private loadConfigs(storage) {
        storage.setItem("api_urls", JSON.stringify((<any>urlsData).apiUrls));
    }

}