import {  Injectable,  EventEmitter, Inject  } from '@angular/core';  
import * as urlsData from 'configs.json';  
import { Subject } from 'rxjs';
// declare the global variables  
declare  var $: any;  

export class SignalrWindow extends Window {  
    $: any;
}

export enum ConnectionState {  
    Connecting = 1,
    Connected = 2,
    Reconnecting = 3,
    Disconnected = 4
}

@Injectable()  
export class SignalRService {  
    // Declare the variables  
    private proxy: any;  
    private proxyName: string = 'Notifier';  
    private connection: any;  
    private tablesList = null;
    private reConnectInterval = 10000;
    private reConnect : any;

    
    public messageReceived: EventEmitter < String >;  
    public connectionEstablished: EventEmitter < Boolean >;
    public updateFardSummary: Subject<Number> = new Subject<Number>();
    public updateMutationSummary: Subject<Number> = new Subject<Number>();
    public updateDistrictDocSummary: Subject<Number> = new Subject<Number>();
    public connectionExists: Boolean;  
    
    constructor(@Inject(SignalrWindow) private window: SignalrWindow) {  
        if (this.window.$ === undefined || this.window.$.hubConnection === undefined) {
            console.log("The variable '$' or the .hubConnection() function are not defined...please check the SignalR scripts have been loaded properly");
        }

        this.connectionEstablished = new EventEmitter < Boolean > ();  
        this.messageReceived = new EventEmitter < String > ();  
        this.connectionExists = false;  
        this.connection = $.hubConnection((<any>urlsData).apiUrls.signalRUrl);  
        this.proxy = this.connection.createHubProxy(this.proxyName);  

        this.connection.stateChanged((state: any) => {
            let newState = ConnectionState.Connecting;

            switch (state.newState) {
                case this.window.$.signalR.connectionState.connecting:
                    newState = ConnectionState.Connecting;
                    console.log("Connecting");
                    break;
                case this.window.$.signalR.connectionState.connected:
                    this.connectionExists = true;
                    clearInterval(this.reConnect);
                    newState = ConnectionState.Connected;
                    console.log("Connected");
                    break;
                case this.window.$.signalR.connectionState.reconnecting:
                    newState = ConnectionState.Reconnecting;
                    console.log("Reconnecting");
                    break;
                case this.window.$.signalR.connectionState.disconnected:
                    newState = ConnectionState.Disconnected;
                    this.connectionExists = false;
                    console.log("Connection dropped");
                    this.reConnect = setInterval(() => {
                            this.reEstablishConnection();
                        }, this.reConnectInterval);
                    break;
            }
        });

        this.registerOnServerEvents();  
        this.startConnection();  
    }  

    public sendNotification(message: String) {  
        this.proxy.invoke('NotifyServer', message);  
    }  
    
    // check in the browser console for either signalr connected or not  
    private startConnection(): void {  
        this.connection.start({ withCredentials: false })
        .done((data: any) => {  
            //console.log('Now connected ' + data.transport.name + ', connection ID= ' + data.id);  
            this.connectionEstablished.emit(true);  
            this.connectionExists = true;  
        }).fail((error: any) => {  
            console.log('Could not connect ' + error);  
            this.connectionEstablished.emit(false);  
            this.connectionExists = false;
        });  
    }  

    private registerOnServerEvents(): void {  
        this.proxy.on('newFard', (data: string) => {
            var newId = parseInt(data);
            this.updateFardSummary.next(newId);
        }); 

        this.proxy.on('newMutation', (data: string) => {
            var newId = parseInt(data);
            this.updateMutationSummary.next(newId);
        });

        this.proxy.on('newDocument', (data: string) => {
            console.log('Document notified');
            var newId = parseInt(data);
            this.updateDistrictDocSummary.next(newId);
        });
    };  

    private reEstablishConnection(): void {
        if (!this.connectionExists) {
            this.startConnection();
        }
    }

}  