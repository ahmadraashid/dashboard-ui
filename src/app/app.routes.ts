import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TehsilComponent } from './tehsils/tehsils.component';
import { DocumentComponent } from './document/document.component';

// Route Configuration
export const routes: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'districts', component: TehsilComponent},
  { path: 'documents', component: DocumentComponent }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);