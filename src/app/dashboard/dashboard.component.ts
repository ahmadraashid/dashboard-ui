import { Component, NgZone } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { FardService } from '../shared/services/fard.service';
import { MutationService } from '../shared/services/mutation.service';
import { SignalRService } from '../shared/services/signalr.service';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { DatePicker } from '../datepicker/datepicker.component';
import { StoreService } from '../shared/services/store.service';
declare  var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
    providers: []
})

export class DashboardComponent {
    public isFardLoading = true;
    public isFardLoaded = false;
    public isMutationLoading = true;
    public isMutationLoaded = false;
    public noFards = false;
    public noMutations = false;
    public initialValue = 1;
    public dated = new Date('mm/dd/yyyy');
    public isDateTimeVisible = false;
    public currentTimeIndex = 0;
    public timeAdjustIndex = 8;
    public timeTailStr = ':00:00.000';

    public dataFilter = {
        Dated: '',
        Time: '',
        IsTimeIncluded: false
    };

    //Fard Stats
    public fardData: Array<any> = null;
    public fardLabels: Array<any> = null;
    public fardChartOptions: any = null;
    public fardChartColors: Array<any> = null;
    public fardChartLegend: boolean = true;
    public fardChartType: string = 'bar';
    public fardDoughnutChartType: string = 'doughnut';
    public fardDoughnutData: Array<any> = null;

    public mutationData: Array<any> = null;
    public mutationLabels: Array<any> = null;
    public mutationChartOptions: any = null;
    public mutationChartColors: Array<any> = null;
    public mutationChartLegend: boolean = true;
    public mutationChartType: string = 'bar';
    public mutationDoughnutChartType: string = 'doughnut';
    public mutationDoughnutData: Array<any> = null;

    public summaryDate: String = '';
    public summaryTime: String = '';

    constructor(public zone: NgZone, public fardService: FardService, 
        public mutationService: MutationService, public signalRService: SignalRService, 
        public storeService: StoreService) {

        var dt = new Date();
        this.summaryDate = this.storeService.getCurrentDate();

        this.dataFilter.Dated = this.summaryDate.toString();
        this.getFardSummary();
        this.getMutationSummary();
        
        
        this.signalRService.updateFardSummary.subscribe((newId) => {
           // if (this.summaryDate == storeService.getCurrentDate()) {
                this.zone.run(() => {
                    this.isFardLoading = true;
                    this.isFardLoaded = false;
                    this.getFardSummary();
                });
            //}
        });

        this.signalRService.updateMutationSummary.subscribe((newId) => {
            //if (this.summaryDate === storeService.getCurrentDate()) {
                this.zone.run(() => {
                    this.isMutationLoading = true;
                    this.isMutationLoaded = false;
                    this.getMutationSummary();
                });
            //}
        });

        this.storeService.openDateTime.subscribe((isOpen) => {
            this.zone.run(() => {
                if (isOpen)
                    this.showDateTime();
            });
        });

    }

    public getFardSummary () {
        this.dataFilter.Dated = this.summaryDate.toString();
        if (this.summaryTime) {
            this.dataFilter.Time = this.summaryTime.toString();
            this.dataFilter.IsTimeIncluded = true;
        } else {
            this.dataFilter.IsTimeIncluded = false;
        }
        
        this.noFards = false;
        this.fardService.getFardSummaries(this.dataFilter).subscribe(results => this.loadFardData(results));
    }

    public getMutationSummary() {
        this.dataFilter.Dated = this.summaryDate.toString();
        if (this.summaryTime) {
            this.dataFilter.Time = this.summaryTime.toString();
            this.dataFilter.IsTimeIncluded = true;
        } else {
            this.dataFilter.IsTimeIncluded = false;
        }
        
        this.noMutations = false;
        this.mutationService.getMutationSummaries(this.dataFilter).subscribe(results => this.loadMutationData(results));
    }

    public showDateTime() {
        this.isDateTimeVisible = true;
    }

    public closeDateTime() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
        this.currentTimeIndex = 0;
    }

    public searchFilteredData() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
        this.currentTimeIndex = 0;

        this.zone.run(() => {
            this.isFardLoading = true;
            this.isFardLoaded = false;
            this.isMutationLoading = true;
            this.isMutationLoaded = false;
            this.getFardSummary();
            this.getMutationSummary();
        });
    }

    public loadFardData(fards): void {
        var fardData = Array();
        var fardLabels = Array();

        if (fards.length == 0) {
            this.isFardLoaded = false;
            this.noFards = true;
        } else {
            fards.forEach(fard => {
                fardData.push(fard.Fards);
                fardLabels.push(fard.District + ' (' + fard.Revenue + ' PKR)');    
            });
    
            this.fardChartOptions = {
                responsive: true, 
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };
            this.fardChartColors = [
                {
                    backgroundColor: [
                        "#FF7360", "#6FC8CE", "#4cc6bb", "#fdd100", "#123ea9"
                    ]
                }
            ];
    
            this.fardDoughnutData = fardData;
            this.fardData = [
                 { data: fardData, label: 'Fard Summaries' },
            ];
            this.fardLabels = fardLabels;
            this.isFardLoaded = true;
        }
        this.isFardLoading = false;
    }

    public loadMutationData(mutations): void {
        var mutationData = Array();
        var mutationLabels = Array();

        if (mutations.length == 0) {
            this.isMutationLoaded = false;
            this.noMutations= true;
        } else {
            mutations.forEach(mutation => {
                mutationData.push(mutation.Mutations);
                mutationLabels.push(mutation.District + ' (' + mutation.Revenue + ' PKR)');    
            });
    
            this.mutationChartOptions = {
                responsive: true, 
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };
            this.mutationChartColors = [
                {
                    backgroundColor: [
                        "#4cc6bb", "#6FC8CE", "#FF7360", "#fdd100", "#123ea9"
                    ]
                }
            ];
    
            this.mutationDoughnutData = mutationData;
            this.mutationData = [
                 { data: mutationData, label: 'Mutation Summaries' },
            ];
            this.mutationLabels = mutationLabels;
            this.isMutationLoaded = true;
        }
        this.isMutationLoading = false;
    }

    onTimeSelection(e): void {
        if (e.from == 0) {
            this.summaryTime = null;
        }

        var timeStr = '';
        var selTime = 0;
        selTime += this.timeAdjustIndex;
        timeStr = this.padTwoDigits(selTime) + this.timeTailStr;
        this.summaryTime = timeStr;
    }

    onDateChange(e): void {
        this.currentTimeIndex = 0;
        var dtArr = e.split('/');
        this.summaryDate = dtArr[2] + '-' + dtArr[0] + '-' + dtArr[1];
    }

    padTwoDigits(number): string {
        return (number < 10 ? '0' : '') + number;
     }

}