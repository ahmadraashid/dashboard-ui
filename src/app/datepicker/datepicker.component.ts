import { forwardRef, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, Component, OnDestroy } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
//import { StoreService } from '../shared/services/store.service';

const DATE_PICKER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatePicker),
  multi: true
};

declare var jQuery: any;

@Component({
  selector: 'datepicker',
  templateUrl: './datepicker.component.html',
  providers: [
    DATE_PICKER_VALUE_ACCESSOR
  ]
})

//var timestamp = new Date().getUTCMilliseconds();
export class DatePicker implements AfterViewInit, ControlValueAccessor, OnDestroy {
  @Input() options: any = {};
  private onTouched = () => { };
  private onChange: (value: string) => void = () => { };

  @Input() value = '';

  constructor() {

  }

  writeValue(date: string) {
    this.value = date;
    jQuery(this.input.nativeElement).datepicker('setDate', date);
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  @Output() dateChange = new EventEmitter();

  @ViewChild('input') input: ElementRef;

  ngAfterViewInit() {
    jQuery(this.input.nativeElement).datepicker(Object.assign({}, this.options, {
      onSelect: (value) => {
        this.value = value;

        this.onChange(value);

        this.onTouched();

        this.dateChange.next(value);
        //this.storeService.setNewDate(value);
      }
    }))
    .datepicker('setDate', this.value);
  }

  ngOnDestroy() {
    jQuery(this.input.nativeElement).datepicker('destroy');
  }
  
}