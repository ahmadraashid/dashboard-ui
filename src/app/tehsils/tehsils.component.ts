import { Component, NgZone } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { TehsilService } from '../shared/services/tehsil.service';
import { SignalRService } from '../shared/services/signalr.service';
import { DatePicker } from '../datepicker/datepicker.component';
import { StoreService } from '../shared/services/store.service';
declare  var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './tehsils.component.html',
    styleUrls: ['./tehsils.component.css'],
    providers: []
})

export class TehsilComponent {
    public districts = Array();
    public selectedDistrict = 'Revenue Summary';
    public isSummaryLoading = false;
    public isSummaryLoaded = false;
    public isDistrictsVisible = true;
    public isDistrictsLoading = true;
    public dateFilter = { "Id": 0, "Dated": "" };
    public isDateTimeVisible = false;
    public dataChangedMessage: string = '';
    public messageDislayTime = 0;
    public dataChanged = false;

    //Chart options
    public summaryData: Array<any> = [];
    public summaryLabels: Array<any> = [];
    public summaryChartOptions: any = null;
    public summaryChartColors: Array<any> = [];
    public summaryChartLegend: boolean = true;
    public summaryChartType: string = 'bar';

    constructor(public zone: NgZone, public tehsilService: TehsilService, 
        public signalRService: SignalRService, 
        public storeService: StoreService) {

            this.getDistricts();
            this.dateFilter.Dated = this.storeService.getCurrentDate();
            this.messageDislayTime = this.storeService.getMessageDisplayTime();
            this.storeService.openDateTime.subscribe((isOpen) => {
                this.zone.run(() => {
                    if (isOpen)
                        this.showDateTime();
                });
            });
    }

    public getDistricts () {
        this.tehsilService.getDistricts().subscribe(results => this.loadDistricts(results));
    }

    public loadDistricts(districts) {
        this.districts = districts;
    }

    public showTehsils(event) {
        this.isDistrictsVisible = false;
        this.isSummaryLoading = true;
        var districtId = this.storeService.getId(event);
        this.dateFilter.Id = districtId;

        this.getSelectedDistrict(districtId);
        this.isSummaryLoading = true;
        this.isSummaryLoaded = false;
        //this.zone.run(() => {
            this.tehsilService.getSelectedDistrictSummary(this.dateFilter)
            .subscribe(results => this.loadTehsilsSummary(results));
        //});
    }

    public loadTehsilsSummary(summary) {
        var tehsilLabels: Array<string> = [];
        var tehsilFardStats: Array<any> = [];
        var tehsilMutationStats: Array<any> = [];
        
        if (summary.RevenueSummary) {
            summary.RevenueSummary.forEach(tehsil => {
                tehsilLabels.push(tehsil.Tehsil + ' (F: ' + tehsil.FardRevenue + ' PKR - M: ' + tehsil.MutationRevenue + ' PKR)');
                tehsilFardStats.push(tehsil.FardsCount);
                tehsilMutationStats.push(tehsil.MutationsCount);
            });
        }

        this.summaryLabels = tehsilLabels;
        this.summaryData = [
            {data: tehsilFardStats, label: 'Fards'},
            {data: tehsilMutationStats, label: 'Mutations'}
        ];
        this.summaryChartType = 'bar';
        this.summaryChartOptions = {
            responsive: true, 
            scaleShowVerticalLines: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        };
        this.summaryChartLegend = true;

        this.isSummaryLoading = false;
        this.isSummaryLoaded = true;
    }

    public getSelectedDistrict(id) {
        if (this.districts.length > 0) {
            var theDistrict = this.districts.filter(function (district) {
                return district.Id === id;
            });

            if (theDistrict) {
                this.selectedDistrict = 'Tehsils Revenue Summary for District ' + theDistrict[0].Name;
            }
        }
    }

    public showDateTime() {
        this.isDateTimeVisible = true;
    }

    public searchFilteredData() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
        this.showDateChangedMessage();
    }

    public showDateChangedMessage() {
        this.dataChangedMessage = 'Date updated, click/tap a district to view updated data.';
        this.dataChanged = true;
        setTimeout(() => {
            this.dataChanged = false;
            this.dataChangedMessage = '';
        }, this.messageDislayTime);
    }

    public closeDateTime() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
    }
    
    onDateChange(e): void {
        var dtArr = e.split('/');
        this.dateFilter.Dated = dtArr[2] + '-' + dtArr[0] + '-' + dtArr[1];
    }

    public showDistricts(): void {
        this.isDistrictsVisible = true;
    }


    
}