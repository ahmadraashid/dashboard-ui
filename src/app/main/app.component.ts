import { Component, NgZone } from '@angular/core';
import { StoreService } from '../shared/services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  public isCalendarVisible = true;
  public currentYear = null;
  
  constructor (public storeService: StoreService, public zone: NgZone) {
    this.currentYear = new Date().getFullYear();
    this.storeService.openDateTime.subscribe((isDateTimeOpen: boolean) => {
            this.zone.run(() => {
              this.setCalendarStatus(isDateTimeOpen);
            });
        });
  }

  public setCalendarStatus(isDateTimeOpen) : void {
    if (isDateTimeOpen) {
      this.isCalendarVisible = false;
    } else {
      this.isCalendarVisible = true;
    } 
  }

  public openDateTimeBox() {
    this.storeService.setDateTimeStatus(true);
  }
  
}
