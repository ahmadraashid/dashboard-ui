import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routing } from './app.routes';
import { ChartsModule } from 'ng2-charts';
import { CoolStorageModule } from 'angular2-cool-storage';
import { IonRangeSliderModule } from "ng2-ion-range-slider";

import { AppComponent } from './main/app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TehsilComponent } from './tehsils/tehsils.component';
import { DocumentComponent } from './document/document.component';
import { DatePicker } from './datepicker/datepicker.component';

import { FardService } from './shared/services/fard.service';
import { MutationService } from './shared/services/mutation.service';
import { TehsilService } from './shared/services/tehsil.service';
import { SignalRService, SignalrWindow } from './shared/services/signalr.service';
import { StoreService } from './shared/services/store.service';
import { DocumentService } from './shared/services/document.service';
declare var $:any;

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TehsilComponent,
    DocumentComponent,
    DatePicker
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing,
    ChartsModule,
    CoolStorageModule.forRoot(),
    IonRangeSliderModule
  ],
  providers: [
    FardService,
    MutationService,
    TehsilService,
    SignalRService,
    { provide: SignalrWindow, useValue: window },
    StoreService,
    DocumentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
