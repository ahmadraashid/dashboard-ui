import { Component, NgZone } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { TehsilService } from '../shared/services/tehsil.service';
import { DocumentService } from '../shared/services/document.service';
import { SignalRService } from '../shared/services/signalr.service';
import { DatePicker } from '../datepicker/datepicker.component';
import { StoreService } from '../shared/services/store.service';
declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './document.component.html',
    //styleUrls: ['./document.component.css'],
    providers: []
})

export class DocumentComponent {
    public districts: Array<any>;
    public tehsils: Array<any>;
    public districtTehsilsList: Array<any>;
    public documentStatusSummary: Array<any>;
    public selectedDistrictSummary = {Id: 0, DocumentSummary: []};
    public selectedTehsilSummary: Array<any>;
    public tehsilDocStatusSummary: Array<any>;
    public documentStates = ['None', 'Received', 'InProcess', 'Pending', 'Completed' ];
    
    public dated = new Date('mm/dd/yyyy');
    public dataChanged = false;
    public documentTypes = null;
    public selectedDistrict = '';
    public selectedTehsil = '';
    public isSummaryLoading = false;
    public isSummaryLoaded = false;
    public isDataAvailable = false;
    public isDateTimeVisible = false;
    public districtView = true;
    public tehsilsView = false;
    public summaryView = false;
    public chartView = false;
    public dateFilter = { "Id": 0, "Dated": "" };
    public disableClass: string = '';
    public dataChangedMessage: string = '';
    public messageDislayTime = 0;

    //Chart options
    public summaryData: Array<any> = [];
    public summaryLabels: Array<any> = [];
    public summaryChartOptions: any = null;
    public summaryChartColors: Array<any> = [];
    public summaryChartLegend: boolean = true;
    public summaryChartType: string = 'bar';

    constructor(public zone: NgZone, public documentService: DocumentService,
        public tehsilService: TehsilService,
        public signalRService: SignalRService,
        public storeService: StoreService) {

        this.getDistricts();
        this.getTehsils();
        this.getDocumentTypes();
        this.dateFilter.Dated = this.storeService.getCurrentDate();
        this.messageDislayTime = this.storeService.getMessageDisplayTime();
        
        this.storeService.openDateTime.subscribe((isOpen) => {
            this.zone.run(() => {
                if (isOpen)
                    this.showDateTime();
            });
        });

        this.signalRService.updateDistrictDocSummary.subscribe((newId) => {
            this.zone.run(() => {
                this.dateFilter.Id = parseInt(newId.toString());
                this.selectedDistrict = this.getSelectedDistrict(this.dateFilter.Id);
                this.dataChangedMessage = 'New data is loading and updating for the district: ' + this.selectedDistrict;
                this.dataChanged = true;
                this.selectedTehsil = '';
                this.tehsilsView = false;
                this.districtView = true;
                this.isSummaryLoading = true;
                this.isDataAvailable = false;
                this.tehsilDocStatusSummary = [];

                setTimeout(() => {
                    this.dataChanged = false;
                    this.dataChangedMessage = '';
                }, this.messageDislayTime);
    
                this.showUpdatedDistrict(newId);
            });
        });
    }

    public showDateTime() {
        this.isDateTimeVisible = true;
    }

    public closeDateTime() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
    }

    public getDistricts() {
        this.tehsilService.getDistricts().subscribe(results => this.loadDistricts(results));
    }

    public getTehsils() {
        this.tehsilService.getTehsils().subscribe(results => this.loadTehsils(results));
    }

    public getDocumentTypes() {
        this.documentService.getDocumentTypes().subscribe(results => this.loadDocumentTypes(results));
    }

    public loadDistricts(districts) {
        this.districts = districts;
    }

    public loadTehsils(tehsils) {
        this.tehsils = tehsils;
    }

    public loadDocumentTypes(docTypes) {
        this.documentTypes = docTypes;
    }

    public showDistricts(event) {
        this.tehsilDocStatusSummary = [];
        this.selectedTehsil = '';
        this.tehsilsView = false;
        this.districtView = true;
        this.summaryView = false;
        this.isDataAvailable = false;
    }

    public showUpdatedDistrict(id) {
        this.dateFilter.Id = id;
        this.documentService.getSelectedDistrictSummary(this.dateFilter)
            .subscribe(results => this.loadTehsilsSummary(results));

        this.districtTehsilsList = this.getDistrictTehsils(id);
    }

    public showTehsils(event) {
        this.dataChanged = false;
        var districtId = this.storeService.getId(event);
        this.tehsilsView = true;
        this.districtView = false;
        this.selectedDistrict = this.getSelectedDistrict(districtId);
        this.districtTehsilsList = this.getDistrictTehsils(districtId);
        this.isSummaryLoading = true;
        this.dateFilter.Id = districtId;
        this.isSummaryLoading = true;
        this.isSummaryLoaded = false;
        this.documentService.getSelectedDistrictSummary(this.dateFilter)
            .subscribe(results => this.loadTehsilsSummary(results));
    }

    public showTehsilDetail(event) {
        var colors = Array();
        var docStatusSummary = [];
        var summaryData = Array();
        var summaryLabels = Array();
        var counter = 0;
        this.selectedTehsil = '';
        this.tehsilsView = false;
        this.chartView = true;
        this.summaryView = true;

        var tehsilId = this.storeService.getId(event);
        var docsSummary = this.selectedDistrictSummary.DocumentSummary.filter(function (summary) {
            return summary.TehsilId == tehsilId;
        });

        docsSummary.forEach(doc => {
            if (counter == 0 && doc.Tehsil)
            {
                this.selectedTehsil = 'Files Summary for Tehsil ' + doc.Tehsil;
            }

            colors.push(this.storeService.getColor(counter));
            ++counter;
            summaryData.push(doc.Count);
            summaryLabels.push(doc.DocumentTitle);

            if (doc.DocStatusSummaries && doc.DocStatusSummaries.length > 0) {
                doc.DocStatusSummaries.forEach(function(summary) {
                    var found = docStatusSummary.filter(function(status) {
                        return status.Status == summary.Status;
                    });

                    if (found.length > 0) {
                        found[0].Count += summary.Count;
                    } else {
                        var objStatus = {Status: 0, Count: 0, Title: ''};
                        objStatus.Status = summary.Status;
                        objStatus.Count = summary.Count;
                        objStatus.Title = this.documentStates[summary.Status];
                        docStatusSummary.push(objStatus);
                    }
                }.bind(this));
            }
        });

        this.isDataAvailable = false;
        this.isSummaryLoading = true;
        this.disableClass = 'disable';

        setTimeout(() => {
            this.tehsilDocStatusSummary = docStatusSummary;
            this.summaryData = [
                {data: summaryData, label: 'Document Summary'}
            ];
            this.summaryLabels = summaryLabels;
            this.summaryChartOptions = {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };
            this.summaryChartType = 'bar';
            this.summaryChartLegend = true;
            this.summaryChartColors = [
                {
                    backgroundColor: colors
                }
            ];

        this.isSummaryLoading = false;
        this.isDataAvailable = true;
        this.disableClass = '';
        }, 1000);     
    }

    public showTehsilsView() {
        this.chartView = false;
        this.summaryView = false;
        this.tehsilsView = true;
    }

    public searchFilteredData() {
        this.isDateTimeVisible = false;
        this.storeService.setDateTimeStatus(false);
        this.showDateChangedMessage();
    }

    public loadTehsilsSummary(summary) {
        this.selectedDistrictSummary = summary;
        this.documentStatusSummary = summary.DocumentStatusSummary;
        this.isSummaryLoading = false;
        this.isSummaryLoaded = true;
    }

    public getDistrictTehsils (districtId) {
        var districtTehsils = this.tehsils.filter(function (tehsil) {
            return tehsil.DistrictId == districtId;
        });
        return districtTehsils;
    }

    public getSelectedDistrict(id) {
        var selectedDistrict = '';
        if (this.districts.length > 0) {
            var theDistrict = this.districts.filter(function (district) {
                return district.Id === id;
            });

            if (theDistrict.length && theDistrict.length > 0) {
                selectedDistrict = theDistrict[0].Name;
            }
            return selectedDistrict;
        }
    }

    public resetChartData() {
        this.summaryData = [];
        this.summaryLabels = [];
        this.summaryChartOptions = null;
        this.summaryChartColors = [];
    }

    public showDateChangedMessage() {
        this.dataChangedMessage = 'Date updated, click/tap a district to view updated data.';
        this.dataChanged = true;
        setTimeout(() => {
            this.dataChanged = false;
            this.dataChangedMessage = '';
        }, this.messageDislayTime);
    }

    onDateChange(e): void {
        var dtArr = e.split('/');
        this.dateFilter.Dated = dtArr[2] + '-' + dtArr[0] + '-' + dtArr[1];
        this.tehsilsView = false;
        this.isDataAvailable = false;
        this.districtView = true;
    }



}