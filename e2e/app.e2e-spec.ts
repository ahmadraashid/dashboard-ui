import { BorDashboardPage } from './app.po';

describe('bor-dashboard App', () => {
  let page: BorDashboardPage;

  beforeEach(() => {
    page = new BorDashboardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
